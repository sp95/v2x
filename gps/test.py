import pickle
from gpsd import GpsPoller
import time

gps_model = pickle.load(open('models/gps.sav', 'rb'))

gpsp = GpsPoller()
gpsp.start()
gps_cords = list()

while True:
    #print(gpsp.get_location())
    lat, lon = gpsp.get_location()
    print("Latitude: ", lat, "Longitude: ", lon)
    '''
    lat, lon = gpsp.get_location()
    print(type(lat))
    lat = append_gps(lat)
    lon = append_gps(lon)
    '''
    gps_cords.append(lat)
    gps_cords.append(lon)
    print("You're in Region {}".format(str(gps_model.predict([gps_cords]))))
    gps_cords = []
    time.sleep(1)
    print("*"*50)
