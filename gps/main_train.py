import time
from gpsd import GpsPoller
import csv

def append_gps(s):
    if(len(str(s))!=8):
        while(len(str(s))!=8):
            s = str(s) + "0"
    return s

gpsp = GpsPoller()
gpsp.start()
gps_cords = list()

while True:
    #print(gpsp.get_location())
    lat, lon = gpsp.get_location()
    print("Latitude: ", lat, "Longitude: ", lon)
    '''
    lat, lon = gpsp.get_location()
    print(type(lat))
    lat = append_gps(lat)
    lon = append_gps(lon)
    '''
    gps_cords.append(lat)
    gps_cords.append(lon)
    print("Got the Coordinates.")
    '''
    with open("cords.csv", 'a') as myfile:
        wr = csv.writer(myfile, quoting=csv.QUOTE_ALL)
        wr.writerow(gps_cords)
    '''
    print("Written to CSV file")
    gps_cords = []
    time.sleep(1)
    print("*"*50)
