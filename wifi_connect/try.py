#!/usr/bin/python
from __future__ import print_function
import time
from wifi import Cell, Scheme

'''
For Vishal:
----------
    - scheme has already been saved. To create a new scheme, refer "https://wifi.readthedocs.io/en/latest/scanning.html#discovering-networks"
        >>> cell = Cell.all('wlan0')[0]
        >>> scheme = Scheme.for_cell('wlan0', 'home', cell, passkey)
        >>> scheme.save()
'''

scheme = Scheme.find('wlan0', 'test')
print(scheme)
t1 = time.time()
print(scheme.activate())
t2 = time.time()
t3 = t2 - t1
print(t3)
