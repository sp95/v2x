# v2x

For Vishal:
-----------
    - Run try.py [Inside wifi_connect folder] to connect to the already saved network.
    - Once the scheme has been saved, add static IP in /etc/network/interfaces file.

To check the network currently connected to:
--------------------------------------------
    - nm-tool | grep \*         (For Ubuntu < 16.04)
    - nmcli -t -f active,ssid dev wifi | egrep '^yes' | cut -d\' -f2    (For Ubuntu >= 16.04)

For gps setup:
-------------
   USB:
   ---
   - sudo service gpsd stop
   - Run the gpsd -b command from help page.
   - Run main.py
  
   Bluetooth:
   ---------
   - sudo service gpsd stop
   - Connect to the Bluetooth
   - hcitool scan
   - sdptool browse 20:34:FB:78:D9:20
   - rfcomm connect /dev/rfcomm1 20:34:FB:78:D9:20 <Channel Number>
   - gpsd -N -n -b /dev/rfcomm1
   - Run main.py

For wpa_supplicant setup:
------------------------

    - wpa remove_interface wlan0
    - run wpa_scan.py

Extra:
-----
    - ctrl+shift+\ for forced quit
